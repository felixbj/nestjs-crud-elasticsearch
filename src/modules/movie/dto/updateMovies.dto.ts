import { IsArray,IsNotEmpty,IsOptional } from "class-validator";

export class UpdateMovieDto {
    @IsNotEmpty() @IsOptional()
    readonly imdbID: string;
    @IsNotEmpty() @IsOptional()
    readonly title: string;
    @IsNotEmpty() @IsOptional()
    readonly plot: string;
    @IsNotEmpty() @IsOptional()
    readonly type: string;
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly genre: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly director: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly writers: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly actors: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly language: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly country: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly countryCode2: [string];
    @IsNotEmpty() @IsOptional()
    @IsArray()
    readonly countryCode3: [string];
    @IsNotEmpty() @IsOptional()
    readonly year: number;
    @IsNotEmpty() @IsOptional()
    readonly released: string;
    @IsNotEmpty() @IsOptional()
    readonly runtime: string;
    @IsNotEmpty() @IsOptional()
    readonly rated: string;
    @IsNotEmpty() @IsOptional()
    readonly awards: string;
    @IsNotEmpty() @IsOptional()
    readonly poster: string;
    @IsNotEmpty() @IsOptional()
    readonly metascore: number;
    @IsNotEmpty() @IsOptional()
    readonly imdbRating: number;
    @IsNotEmpty() @IsOptional()
    readonly imdbVotes: number;
}