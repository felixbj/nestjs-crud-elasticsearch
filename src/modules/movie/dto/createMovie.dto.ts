import { IsArray, IsNotEmpty } from "class-validator";

export class CreateMovieDto {
    @IsNotEmpty()
    readonly imdbID: string;
    @IsNotEmpty()
    readonly title: string;
    @IsNotEmpty()
    readonly plot: string;
    @IsNotEmpty()
    readonly type: string;
    @IsNotEmpty()
    @IsArray()
    readonly genre: [string];
    @IsNotEmpty()
    @IsArray()
    readonly director: [string];
    @IsNotEmpty()
    @IsArray()
    readonly writers: [string];
    @IsNotEmpty()
    @IsArray()
    readonly actors: [string];
    @IsNotEmpty()
    @IsArray()
    readonly language: [string];
    @IsNotEmpty()
    @IsArray()
    readonly country: [string];
    @IsNotEmpty()
    @IsArray()
    readonly countryCode2: [string];
    @IsNotEmpty()
    @IsArray()
    readonly countryCode3: [string];
    @IsNotEmpty()
    readonly year: number;
    @IsNotEmpty()
    readonly released: string;
    @IsNotEmpty()
    readonly runtime: string;
    @IsNotEmpty()
    readonly rated: string;
    @IsNotEmpty()
    readonly awards: string;
    @IsNotEmpty()
    readonly poster: string;
    @IsNotEmpty()
    readonly metascore: number;
    @IsNotEmpty()
    readonly imdbRating: number;
    @IsNotEmpty()
    readonly imdbVotes: number;
}
/*
{
    "imdbID":"tt0221073",
    "title":"Chopper",
    "plot":"Chopper tells the intense story of Mark \"Chopper\" Read, a legendary criminal who wrote his autobiography while serving a jail sentence in prison. His book, \"From the Inside\", upon which the film is based, was a best-seller.",
    "type":"movie",
    "genre":["Biography","Comedy","Crime"],
    "director":["Andrew Dominik"],
    "writers":["Mark Brandon Read (books)","Andrew Dominik"],
    "actors":["Eric Bana","Simon Lyndon","David Field","Dan Wyllie"],
    "language":["English"],
    "country":["Australia"],
    "countryCode2":["AU"],
    "countryCode3":["AUS"],
    "year":2000,
    "released":"2000-08-03T00:00:00Z",
    "runtime":"94 min",
    "rated":"Unrated",
    "awards":"12 wins & 14 nominations.",
    "poster":"http://ia.media-imdb.com/images/M/MV5BNTc5Mzc3Njc4M15BMl5BanBnXkFtZTYwODg1MTA5._V1_SX300.jpg",
    "metascore":65,
    "imdbRating":7.2,
    "imdbVotes":28096
}
*/