import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { Module } from '@nestjs/common';
import { ElasticsearchModule } from '@nestjs/elasticsearch';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ELASTICSEARCH_CONFIG } from '../../config/constants'

@Module({
    imports: [
        ElasticsearchModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => configService.get(ELASTICSEARCH_CONFIG)
        })
    ],
    controllers: [
        MovieController,
    ],
    providers: [
        MovieService
    ],
})
export class MovieModule { }
