import { Injectable, NotFoundException } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { serialize } from 'class-transformer';
import { INDICE_MOVIES, TYPE_MOVIES } from 'src/config/constants';
import { CreateMovieDto, UpdateMovieDto } from './dto';

@Injectable()
export class MovieService {
    constructor(
        private readonly elasticsearchService:ElasticsearchService
    ){}
    async getAll(){
        return await this.elasticsearchService.search({
            index:INDICE_MOVIES,
            body:{
                "query": {
                    "match_all": {}
                }
                , "_source": {
                  "includes": ["title"]
                }
            }
        });
    }
    async getTitle(movieTitle:string){
        return await this.elasticsearchService.search({
            index:INDICE_MOVIES,
            body:{
                "query": {
                    "match": { "title":`"${movieTitle}"` }
                }
                , "_source": {
                  "includes": ["title"]
                }
            }
        });
    }
    async createOne(movieDto:CreateMovieDto){
        const bulk = [];
        bulk.push({ 
            index: {_index:INDICE_MOVIES, _type:TYPE_MOVIES,_id:movieDto.imdbID} 
        });
        bulk.push(movieDto);
        return await this.elasticsearchService.bulk({
            index:INDICE_MOVIES,
            body:bulk
        });
    }
    async editOne(idMovie:string,movieDto:UpdateMovieDto){
        const f = await this.elasticsearchService.search({
            index:INDICE_MOVIES,
            body:{
                "query": {
                    "match": { "_id":idMovie }
                }
                , "_source": {
                  "includes": ["title"]
                }
            }
        });
        if(!f.body['hits']['total']['value']) throw new NotFoundException();
        const bulk = [];
        bulk.push({ 
            index: {_index:INDICE_MOVIES, _type:TYPE_MOVIES,_id:idMovie} 
        });
        bulk.push(movieDto);
        return await this.elasticsearchService.bulk({
            index:INDICE_MOVIES,
            body:bulk
        });
    }
    async deleteOne(idMovie:string){
        const f = await this.elasticsearchService.search({
            index:INDICE_MOVIES,
            body:{
                "query": {
                    "match": { "_id":idMovie }
                }
                , "_source": {
                  "includes": ["title"]
                }
            }
        });
        if(!f.body['hits']['total']['value']) throw new NotFoundException();
        return await this.elasticsearchService.delete({
            index:INDICE_MOVIES,
            id:idMovie
        })
    }
}