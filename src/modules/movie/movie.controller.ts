import { Body, Controller,Delete,Get,Param,Post,Put } from '@nestjs/common';
import { CreateMovieDto,UpdateMovieDto } from './dto';
import { MovieService } from './movie.service';

@Controller('movies')
export class MovieController {
    constructor(
        private readonly movieService:MovieService
    ){}
    @Get()
    async getAllMovies() {
        const data = await this.movieService.getAll();
        return {
            message: 'Todos las peliculas',
            data
        }
    }
    @Get('title/:title')//Buscar texto
    async getTitleMovies(@Param('title') movieTitle:string) {
        const data = await this.movieService.getTitle(movieTitle);
        return {
            message: `Una pelicula donde el titulo contiene ${movieTitle}`,
            data
        }
    }
    @Post()
    async createOneMovie(@Body() movieDto:CreateMovieDto){
        const data = this.movieService.createOne(movieDto);
        return {
            message: `Una pelicula creada con el  el titulo ${movieDto.title}`,
            data
        }
    }
    @Put(':id')
    async editOneUser(@Param('id') idMovie:string,@Body() movieDto:UpdateMovieDto){
        const data = await this.movieService.editOne(idMovie,movieDto);
        return {
            message: `Edita una pelicula con el id ${idMovie}`,
            data
        };
    }
    @Delete(':id')
    async deleteOneMovie(@Param('id') idMovie:string){
        const data = await this.movieService.deleteOne(idMovie);
        return {
            message: `Elimina una pelicula con el id ${idMovie}`,
            data
        };
    }
}
