import { registerAs }from '@nestjs/config';
import { ElasticsearchModuleOptions } from '@nestjs/elasticsearch';

function createElasticsearchOptions():ElasticsearchModuleOptions{
    return { 
        node:process.env.ELASTICSEARCH_NODE  
    };
}

export default registerAs('database',() =>({
    config: createElasticsearchOptions()
}));// funcion para regstrar un namespace, congurar una varble de entorno llamada database