import { MovieModule } from './modules/movie/movie.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import databaseConfig from './config/database.config';

@Module({
  imports: [
    MovieModule,
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos --- varble de entorno llamada database
      isGlobal: true,
      envFilePath: '.env'
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
// crear el directorio config y un archivo constants.ts
//  Para almacenar las variables de configuración en el entorno npm i --save @nestjs/config e importamos ConfigModule
//  Configuramos ConfigModule
// Para manejar los datos instalar npm i --save @nestjs/elasticsearch @elastic/elasticsearch e importamos a searchmodule
// crear directorio modules/movie y modules/movie/dto
// Para magejar los dto intalar npm i --save class-validator class-transformer
